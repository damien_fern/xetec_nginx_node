const validator = {
    '*.customerName': {
        in: ['body'],
        exists: {
            errorMessage: 'customerName is required'
        },
        isString: {
            errorMessage: 'customerName must be string'
        },
        toString: true
    },
    '*.serverName': {
        in: ['body'],
        exists: {
            errorMessage: 'serverName is required'
        },
        isString: {
            errorMessage: 'serverName must be string'
        },
        toString: true
    },
    '*.locations': {
        in: ['body'],
        exists: {
            errorMessage: 'locations is required'
        },
        isArray: {
            errorMessage: 'locations must be an array'
        },
        toArray: true
    },
    '*.locations.*.value': {
        in: ['body'],
        exists: {
            errorMessage: 'locations.*.value in locations is required'
        },
        isString: {
            errorMessage: 'locations.*.value must be a string'
        },
        matches: {
            options: '^\/.*$',
            errorMessage: 'locations.*.value must begin by /'
        },
        toString: true
    },
    '*.locations.*.proxy_pass': {
        in: ['body'],
        exists: {
            errorMessage: 'locations.*.proxy_pass in locations is required'
        },
        isURL: {
            errorMessage: 'locations.*.proxy_pass must be an URL'
        },
        toString: true
    },
    '*.sslTimeOut': {
        in: ['body'],
        exists: {
            errorMessage: 'sslTimeOut is required'
        },
        isInt: {
            errorMessage: 'sslTimeOut must be an number'
        },
        toInt: true
    },

}

module.exports = validator
