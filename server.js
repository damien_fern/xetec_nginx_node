const express = require('express')
const { checkSchema, validationResult } = require('express-validator')
const cors = require('cors')
const app = express()
const bodyParser= require('body-parser')
const multer = require('multer')
const fs = require('fs')
const nginxConf = require('nginx-conf')
const customValidator = require('./createConfigValidator')

app.use(bodyParser.urlencoded({extended: true}))
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads')
  },
  filename: function (req, file, cb) {
    cb(null, 'ConfFileUploaded-' + new Date().toISOString() + '.conf')
  }
})
var upload = multer({ storage: storage })
app.use(cors())

app.use(express.json())
const port = 8080
// const validation = [
//   check('*.customerName').exists().isString(),
// ]

app.post('/createconf', checkSchema(customValidator), (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  }
  var generatedContent = ''
  fs.readFile('./nginx_proxy_header.conf.dist', 'utf8', function(err, contents) {
    // console.log(contents)
    generatedContent += contents
    req.body.forEach(oneConf => {
      var nameForfile = oneConf.customerName.replace(' ', '_').replace('(', '').replace(')', '')
      var clientID = oneConf.serverName.match(/^(.*)\.(.*)\.(.*)$/i)[1]
      generatedContent += 
`
#########################
#	`+ oneConf.customerName +`	#
#########################
server{
    listen 443;
    server_name `+ oneConf.serverName +`;
    ssl_session_cache builtin:1000 shared:SSL:`+ oneConf.sslTimeOut +`m;

`

  oneConf.locations.forEach(oneLocation => {
    var valueWithoutSlash = oneLocation.value.match(/^\/(.*)$/i)[1]
    var access_log = '/var/log/nginx/'+ clientID +'_' + nameForfile + '_flexmaint_' + valueWithoutSlash + '.log'

    generatedContent += `
    location `+ oneLocation.value +` {
        proxy_pass "`+ oneLocation.proxy_pass +`";
        access_log ` + access_log + `;
    }
`
  })
  
    generatedContent +=`}
`

    });
    fs.writeFile('./confs/confGenerated-' + new Date().toISOString() + '.conf', generatedContent, function (err) {
      if (err) throw err
      console.log('File is created successfully.')
    })
    res.send(generatedContent)
  })
})

app.get('/',(req, res, next) => {
  res.send('Root for Xetec Proxy Config Generator')
})

app.post('/uploadfile', upload.single('fileConf'), (req, res, next) => {
  const file = req.file
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
  var path = file.path
  res.setHeader('Content-Type', 'application/json')
  nginxConf.NginxConfFile.create(path, function (err, conf) {
    var serversForJSONTransformation = []
    if (err) {
        console.log(err)
        return
    }
    for (myProperty in conf.nginx.server) {
      var oneServer = conf.nginx.server[myProperty]
      if (typeof oneServer.location !== 'undefined' && oneServer.location.constructor == Object) {
        var oldLocation = oneServer.location
        oneServer.location = [oldLocation]
      }
      var obj = recursiveFunction(oneServer)

      if ('server_name' in obj 
        && obj.server_name != '_') {

        var name = 'unknown'
        if (oneServer.hasOwnProperty('_comments') && typeof oneServer['_comments'] != undefined) {
          let commentName = oneServer._comments[1]
          let arrayComment = commentName.match(/^(.*)#$/)
          name = arrayComment[1].trim()
        }
        name = name.charAt(0).toUpperCase() + name.slice(1)
        var sessionValue = 60
        if (obj.hasOwnProperty('ssl_session_cache') && typeof obj['ssl_session_cache'] != undefined) {
          let sslRawValue =  obj.ssl_session_cache
          sessionValue = sslRawValue.match(/builtin:1000 shared:SSL:([0-9]+)/)[1]
        }
        var aConf = {
          customerName: name,
          serverName: obj.server_name,
          sslTimeOut: sessionValue,
          locations: []
        }
        obj.location.forEach(oneLocation => {
          var proxyPass = oneLocation.proxy_pass;
          if (proxyPass.charAt(0) === '"' && proxyPass.charAt(proxyPass.length -1) === '"')
          {
            proxyPass = proxyPass.substr(1, proxyPass.length -2)
          }
          aConf.locations.push({
            value: oneLocation.value,
            proxy_pass: proxyPass
          })
        })
        serversForJSONTransformation.push(aConf)
      }
    }
    res.json(serversForJSONTransformation)
  })
  
})

app.listen(port, function () {
  console.log('App listening on port 8080!')
})

// function recursiveFunction(object) {
//   for (var property in object) {
//     if (object.hasOwnProperty(property) && object[property] != null) {
//       if (object[property].constructor == Object) {
//         recursiveFunction(object[property]);
//       } else if (object[property].constructor == Array) {
//         for (var i = 0; i < object[property].length; i++) {
//           recursiveFunction(object[property][i]);
//         }
//       } else if (object[property].constructor != Function) {
        
//         return object[property]._value;
//       }
//     }
//   }
// }

function recursiveFunction(myObject) {
  var newObject = {}
  // console.log(Object.getOwnPropertyNames(myObject))
  for (var key in myObject) {
    if (myObject.hasOwnProperty(key) && typeof myObject[key] != undefined) {
      var element = myObject[key]
      if (element.constructor != Function) {
        if (element.constructor == Object) {
          if (element._value != "") {
            newObject[key] = element._value
          } else {
            newObject[key] = recursiveFunction(element)
          }
        } else if (element.constructor == Array) {
          newObject[key] = []
          for (var i = 0; i < element.length; i++) {
            var currentElementInFor = element[i]
            var newElementArray = Object.assign(
            {
              value: currentElementInFor._value
            }, recursiveFunction(currentElementInFor))
            newObject[key].push(newElementArray) 
          }
        } 
      }
    }
  }
  // var newObject = {};
  // var nameIndex = "";
  // for (nameIndex in object) {
  //   console.log(object);
  //   if (nameIndex.charAt(0) != "_" 
  //   && object.hasOwnProperty(nameIndex) 
  //   && typeof object[nameIndex] == "Object") {
  //     newObject[nameIndex] = recursiveFunction(object[nameIndex]);
  //   }
  // }
  return newObject
}